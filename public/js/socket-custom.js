var socket = io();
        
// Escuchar
socket.on('connect', function() {
    console.log('Conectado al servidor!');
});

socket.on('disconnect', function() {
    console.log('Perdimos conexión con el servidor! :(');
});

// Enviar info
socket.emit('enviarMensaje', {
    //usuario: 'Victor',
    mensaje: 'Hola mundano!'
}, function( resp ) {
    console.log('Respuesta SERVER: ', resp);
});

// Escuchar info
socket.on('enviarMensaje', function(mensaje) {
    console.log(mensaje);
});